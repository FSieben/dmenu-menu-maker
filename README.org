#+title: (d)menu-maker
#+author: Frank Siebenmorgen
#+OPTIONS: html-style:nil
#+OPTIONS: toc:nil ^:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="layout.css" />
* Table of Content
- [[(d)menu-maker][(d)menu-maker]]
- [[Dependence][Dependence]]
- [[Run (d)menu-maker][Run (d)menu-maker]]
- [[Instruction][Instruction]]
- [[Knowing bugs][Knowing bugs]]

* (d)menu-maker
Create a menu structure for dmenu (rofi). The menu is based on bash scripts. The program will be automate generate these scripts. The GUI will be implemented in python tkinter.
#+CAPTION: Example
[[./picture/dmenu-menu.jpg]]

* Dependence
+ python 3

* Run (d)menu-maker
Clone the git repository and make /menu_maker.py/ executable. Run the program with /menu_maker.py/.

* Instruction
With *menu_maker* you can create a menu which can execute with /dmenu/.

** Menu files
The main menu and all sub-menus will be saved in separated files. These files are bash scripts and each file can be executed (make sure the files are executable). The file /main_menu.sh/ contain the main menu which has to be started to use the whole menu.

** Program menu
#+CAPTION: Program menu
| Menu   | Entry   | Description                                  |
|--------+---------+----------------------------------------------|
| File   | New     | Create new menu                              |
|        | Load    | Load a menu                                  |
|        | Save    | Save the menu (without asking for directory) |
|        | Save as | Save the menu (asking for directory)         |
|        | Exit    | Exit the program                             |
|--------+---------+----------------------------------------------|
| Option | Option  | Configure the menu                           |


*** Option menu
In the option window are two points to set.
1. /directory/: Set the directory where the bash scripts will be execute.

   e.g.: ~$HOME/.config/menu~
2. /dmenu or rofi/: Set the command for dmenu or rofi.

   e.g.: ~dmenu -bw 0 -l 20~
** Edit menu
#+CAPTION: menu-maker.py
[[./picture/dmenu-maker.jpg]]
*** Tree view
On the left side is the tree view of the menu. This will show the structure of the menu. A click with the mouse will select an entry and it can be edited.
*** In the middle
#+CAPTION: Button description
| Button | Description                                  |
|--------+----------------------------------------------|
| New    | Add a new entry to the menu.                 |
|--------+----------------------------------------------|
| Up     | Move entry up.                               |
| Left   | Move entry left.                             |
| Right  | Move entry right (if next entry a sub-menu). |
| Down   | Move entry Down.                             |
|--------+----------------------------------------------|
| Delete | Delete selected entry.                       |
*** Edit entry (On the right)
#+CAPTION: Edit
| Name       | Description                                                                                                                                                   |
|------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Name       | Name of the entry                                                                                                                                             |
|------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Command    | If the command property selected you can write your command that will be executed if the entry is selected. E.g.: ~firefox &~                                 |
|------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Properties | - Command: You can set your own command in /Command/. E.g.: ~firefox &~                                                                                       |
|            | - Submenu: Set the entry as a sub-menu. This is the beginning of a new menu. You can add new entries to this sub-menu. This menu will be saved in a new file. |
|            | - Back: Set the entry as a back button. This entry will lead you to one menu up.                                                                              |
|------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Apply      | Save the configuration for the entry.                                                                                                                         |
| Cancel     | Cancel the configuration an restore the old configuration of the entry.                                                                                       |

* Knowing bugs [0/2]
- [ ] Load empty menu will run into an error.
- [ ] Save menu if you start a new menu run into an error.
