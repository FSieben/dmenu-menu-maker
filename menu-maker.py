#!/usr/bin/env python3

import tkinter as tk
from tkinter import ttk

from window import Window

# Create the root window.
root = tk.Tk()
root.geometry("1000x600")
root.minsize(600, 350)

# Creation of an instance
app = Window(root)

root.mainloop()
