#!/usr/bin/env python3


class menu_head:
    """Head of the menu."""

    last_id = 0

    def __init__(self):
        """Init menu."""
        self.next_node = None

    def append(self, node):
        """Append a not to the linked list."""
        if self.next_node is None:
            self.next_node = node
            menu_head.last_id += 1
            self.next_node.set_id(menu_head.last_id)
        else:
            temp_node = self.next_node
            while temp_node.next_node is not None:
                temp_node = temp_node.get_next_node()
            temp_node.next_node = node
            menu_head.last_id += 1
            temp_node.next_node.set_id(menu_head.last_id)

    def print_menu(self, indent=0):
        """Print menu."""
        temp_node = self.next_node
        while temp_node is not None:
            print(" " * indent + temp_node.get_name() +
                  " - ID: " + str(temp_node.get_id()))
            if temp_node.get_submenu() is True:
                temp_node.next_menu.print_menu(indent+2)
            temp_node = temp_node.next_node

    def search(self, node_id):
        """Search in list for an id and return the node."""
        search_list = []
        if not self.next_node:
            return None
        else:
            temp_node = self.next_node

        # print(f"Start Search-While-Schleife ({node_id}):")
        while node_id:
            # print(temp_node.get_name())
            if temp_node.get_id() is int(node_id):
                # print("Eins")
                return temp_node
            if temp_node.get_submenu():
                # print("Zwei")
                if temp_node.next_menu.next_node:
                    search_list.append(temp_node)
            if temp_node.get_next_node() is not None:
                # print("Drei")
                temp_node = temp_node.get_next_node()
            elif len(search_list):
                # print("Vier")
                temp_node = search_list.pop().next_menu.next_node
            else:
                # print("Fünf")
                return None
        return None

    def insert(self, node, iid, is_submenu):
        """Insert a new node.

        Insert a node behind the node with id=iid. Is prev_id=None
        insert node behind first node. If is_submenu=True then insert node
        as first child of iid.
        Arguments:
            node: node to insert.
            iid: Insert node behind iid.
            is_submenu: True/False. If true insert as submenu.
        """
        # print(f"Start insert ({iid}):")
        new_node = node
        if iid:
            # print("eins")
            previous_node = self.search(iid)
            if not new_node.get_id():
                # print("zwei")
                menu_head.last_id += 1
                new_node.set_id(menu_head.last_id)
            if is_submenu:
                # print("drei")
                new_node.next_node = previous_node.next_menu.next_node
                previous_node.next_menu.next_node = new_node
                return new_node.get_id()
            else:
                # print("vier")
                new_node.next_node = previous_node.next_node
                previous_node.next_node = new_node
                return menu_head.last_id
        else:
            # print("fünf")
            if not new_node.get_id():
                menu_head.last_id += 1
                new_node.set_id(menu_head.last_id)
            new_node.next_node = self.next_node
            self.next_node = new_node
            return new_node.get_id()

    def move(self, start_id, dest_id):
        """Move entry from start_id ot dest_id.

        start_id: Id of previos element.
        dest_id: Id of the new previous element.
        """
        # print(f"Start move ({start_id} - {dest_id}):")
        if start_id[0]:
            start_entry = self.search(start_id[0])

            if not start_id[1]:  # If entry to be moved not the first child
                temp_entry = start_entry.get_next_node()
                start_entry.set_next_node(temp_entry.get_next_node())
            else:  # Entry to be moved is the first childe
                if start_entry.next_menu:
                    temp_entry = start_entry.next_menu.next_node
                    start_entry.next_menu.next_node = temp_entry.get_next_node()
        else:
            temp_entry = self.next_node
            self.next_node = temp_entry.get_next_node()

        # Delete link to next node
        temp_entry.set_next_node(None)
        self.insert(temp_entry, dest_id[0], dest_id[1])

    def delete(self, prev_iid, is_submenu):
        """Delete the entry with id=iid."""
        if prev_iid:
            temp_entry = self.search(prev_iid)
            if is_submenu:
                delete_entry = temp_entry.next_menu.next_node
                temp_entry.next_menu.next_node = delete_entry.next_node
            else:
                delete_entry = temp_entry.next_node
                temp_entry.next_node = delete_entry.next_node
        else:
            delete_entry = self.next_node
            self.next_node = delete_entry.next_node

        del delete_entry


class menu_node:
    """Menu entry."""

    def __init__(self):
        """Init entry."""
        self.node_id = 0
        self.next_node = None
        self.next_menu = None
        self.submenu = False
        self.backmenu = False
        self.name = ""
        self.command = ""

    def set_backmenu(self, is_backmenu):
        """Set the backmenu. Shows if the entry command goes one menu back."""
        self.backmenu = is_backmenu

    def get_backmenu(self):
        """Return if the entry goes one menu back."""
        return self.backmenu

    def set_next_menu(self, menu):
        """Override next menu."""
        self.next_menu = menu

    def get_next_menu(self):
        """Get next menu."""
        return self.next_menu

    def set_name(self, name):
        """Set name of node."""
        self.name = name

    def set_command(self, command):
        """Set command for node."""
        self.command = command

    def set_submenu(self, submenu):
        """Set submenu of node."""
        self.submenu = submenu

    def set_id(self, node_id):
        """Set node id."""
        self.node_id = node_id

    def get_next_node(self):
        """Get next node."""
        return self.next_node

    def set_next_node(self, node):
        """Set next node."""
        self.next_node = node

    def get_name(self):
        """Get name."""
        return self.name

    def get_command(self):
        """Get command."""
        return self.command

    def get_submenu(self):
        """Get submenu."""
        return self.submenu

    def get_id(self):
        """Get node id."""
        return self.node_id
