#!/usr/bin/bash
#/usr/bin/bash -x start a debug mode

directory="$HOME/.config/dmenu/appmenu"

menu(){
    printf "Zurück\n"
    printf "Libreoffice\n"
    printf "Paperwork\n"
    printf "Thunderbird\n"
    }

choice=$(menu | dmenu -bw 0 -l 20 -fn "mononoki nerd font")

case $choice in
    "Libreoffice")
	libreoffice  &
	;;
    "Paperwork")
	paperwork-gtk &
	;;
    "Thunderbird")
	thunderbird &
	;;
    "Zurück")
	$directory/main_menu.sh &
	;;
    *)
	;;
esac
