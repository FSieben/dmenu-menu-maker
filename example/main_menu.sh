#!/usr/bin/bash
#/usr/bin/bash -x start a debug mode

directory="$HOME/.config/dmenu/appmenu"

menu(){
    printf "Internet\n"
    printf "Office\n"
    printf "Graphic\n"
    printf "System\n"
    printf "Games\n"
    printf "Fun\n"
    }

choice=$(menu | dmenu -bw 0 -l 20 -fn "mononoki nerd font")

case $choice in
    "Internet")
	$directory/internet_menu.sh &
	;;
    "Office")
	$directory/office_menu.sh &
	;;
    "Graphic")
	$directory/graphic_menu.sh &
	;;
    "System")
	$directory/system_menu.sh &
	;;
    "Games")
	$directory/games_menu.sh &
	;;
    "Fun")
	$directory/fun_menu.sh &
	;;
    *)
	;;
esac
