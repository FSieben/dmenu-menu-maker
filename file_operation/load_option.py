#!/usr/bin/env python3
import json
from .save_option import save_option


def load_option():
    """Load the option from option.json."""
    option = {}
    try:
        with open("option.json", "r") as f:
            option = json.load(f)
    except FileNotFoundError:
        option.update(
            {"directory": "$HOME/.config./dmenu/appmenu",
            "dmenurofi": 'dmenu -bw 0 -l 20 -fn "mononoki nerd font"'}
        )
        save_option(option)

    if "directory" not in option:
        option.update(
            {"directory": "$HOME/.config./dmenu/appmenu",
            "dmenurofi": 'dmenu -bw 0 -l 20 -fn "mononoki nerd font"'}
        )
        save_option(option)
    return option
