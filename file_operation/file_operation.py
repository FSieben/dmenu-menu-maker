#!/usr/bin/env python3
from lists import menu_head, menu_node
from .load_option import load_option

import re


def read_file(path, list_head):
    """Open a file and read out the menu entries."""
    """
    Function open a file and read out the menu entries.
    """
    file_state = 0

    file = open(path, "r")
    for line in file:
        # print(line)
        if file_state == 0:
            if "case" in line:
                file_state = 10
        elif file_state == 10:
            if "*)" in line:
                break
            else:
                """Get entry name."""
                split_line = re.search('(?<=\")(.*?)(?=\")', line).group()
                new_node = menu_node()
                new_node.set_name(split_line)
                file_state = 20
        elif file_state == 20:
            """Get command."""
            if "$directory/" in line:  # Submenu
                split_line = line.split("/")
                # Remove at the end of the line all spaces, & and \n
                split_line = re.sub("\s*&\n$", "", split_line[1])
                if new_node.get_name().casefold() in split_line.lower():
                    new_node.set_submenu(True)
                    new_node.set_backmenu(False)
                else:
                    new_node.set_submenu(False)
                    new_node.set_backmenu(True)
                new_node.set_command(split_line)
            else:  # Command
                # Remove at the end of the line all spaces, & and \n
                # and at begining of the line \t
                split_line = re.sub("(^\t\s*)|(\s*&\n$)", "", line)
                new_node.set_submenu(False)
                new_node.set_command(split_line)
            list_head.append(new_node)
            file_state = 30
        elif file_state == 30:
            file_state = 10

    file.close()
    return list_head


def load_menu(path=None, menu=None):
    """Read out the whole menu."""
    """First level of the menu has to be read out already.
    The function will read the submenus from the bash scripts."""
    if menu != None:
        temp_menu = menu.next_node

    if path:
        load_path = path
    else:
        load_path = "./"

    while temp_menu is not None:
        if temp_menu.get_submenu() is True:
            if temp_menu.get_name().casefold() in temp_menu.get_command():
                temp_menu.next_menu = menu_head()
                temp_menu.next_menu = read_file(load_path +
                                                temp_menu.get_command(), # [:-1],
                                                temp_menu.next_menu)
                load_menu(load_path, temp_menu.next_menu)

        temp_menu = temp_menu.next_node


def save_menu(path=None, name="main_menu.sh", menu=None):
    """Save the menu into files."""
    if not menu:
        return

    # Load option
    option = load_option()

    # Set path
    if not path:
        save_path = "./"
    else:
        save_path = path

    # Write into file
    with open(f"{save_path}{name}", "w") as f:
        f.write("#! /usr/bin/bash\n\n")
        f.write(f"directory=\"{option['directory']}\"\n\n")
        f.write("menu(){\n")
        temp_menu = menu.next_node
        while temp_menu:
            f.write(f"\tprintf \"{temp_menu.get_name()}\\n\"\n")
            temp_menu = temp_menu.get_next_node()
        f.write("\t}\n\n")
        f.write(f"choice=$(menu | {option['dmenurofi']})\n\n")
        f.write("case $choice in\n")
        temp_menu = menu.next_node
        submenu = []
        while temp_menu:
            f.write(f"\t\"{temp_menu.get_name()}\")\n")
            if temp_menu.get_submenu():
                directory = "$directory/"
                submenu.append([temp_menu.next_menu, temp_menu.get_command().lower()])
            elif temp_menu.get_backmenu():
                directory = "$directory/"
            else:
                directory = ''
            f.write(f"\t{directory}{temp_menu.get_command().lower()} &\n")
            f.write("\t;;\n")
            temp_menu = temp_menu.get_next_node()
        f.write("\t*)\n")
        f.write("\t;;\n")
        f.write("esac")

    while submenu:
        next_menu = submenu.pop()
        save_menu(path=save_path, name=next_menu[1], menu=next_menu[0])
