#!/usr/bin/env python3
import json


def save_option(option: dict):
    """Save the option into a json file."""
    with open("option.json", "w") as f:
        json.dump(option, f)
