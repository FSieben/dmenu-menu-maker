#!/usr/bin/env python3
import tkinter as tk
from tkinter import ttk

from .workspace import Workspace_frame
from .treeview import myTreeview
from lists import menu_node
from lists import menu_head


class Edit_frame(Workspace_frame):
    """Create the edit frame."""

    def __init__(self, master: tk.Tk, actual_frame: tk.Frame, menu=None):
        """Init for the class."""
        super().__init__(master, actual_frame)
        self.actual_entry = None
        self.menu = menu
        self.create_workspace()

    def create_workspace(self):
        """Create the workspace to edit the menu."""
        # Create frames for separate region
        self.treeview_width = 300
        self.frame_treeview = tk.Frame(self, width=self.treeview_width)
        self.frame_treeview.grid(column=0, row=0, sticky="ns")
        self.frame_navigation = tk.Frame(self, width=80)
        self.frame_navigation.grid(column=1, row=0, sticky="ns")
        self.frame_editregion = tk.Frame(self, width=300)
        self.frame_editregion.grid(column=2, row=0, sticky="nesw")

        # Configure row and column to strech and use the whole space.
        self.rowconfigure(0, weight=1)
        self.columnconfigure(2, weight=1)

        # Create the treeview region.
        self.widget_tree = myTreeview(self.frame_treeview)
        if self.menu:
            self.widget_tree.set_menu(self.menu)
            self.widget_tree.fill_treeview()
        self.widget_tree.column("#0", width=50, stretch=True)
        self.widget_tree.heading("#0", text="Menu", anchor=tk.W)
        self.widget_tree.place(relheight=1.0, relwidth=1.0, width=-15, height=-15)
        self.widget_tree.bind("<ButtonRelease-1>", self.release_click_treeview)

        self.yscrollbar_tree = ttk.Scrollbar(self.frame_treeview,
                                             orient="vertical",
                                             command=self.widget_tree.yview)
        self.yscrollbar_tree.place(anchor="ne", relx=1.0, rely=0.0, relheight=1.0, height=-15)
        self.yscrollbar_tree.bind("<Button1-Motion>", self.change_size_treeview)

        self.xscrollbar_tree = ttk.Scrollbar(self.frame_treeview,
                                             orient="horizontal",
                                             command=self.widget_tree.xview)
        self.xscrollbar_tree.place(anchor="sw", relx=0.0, rely=1.0, relwidth=1.0, width=-15)
        self.widget_tree.configure(xscrollcommand=self.xscrollbar_tree.set)
        self.widget_tree.configure(yscrollcommand=self.yscrollbar_tree.set)

        # Create the navigation region
        self.widget_button_tree_new = ttk.Button(self.frame_navigation,
                                                 text="New",
                                                 command=self.button_tree_new)
        # self.widget_button_tree_new.grid(column=0, row=0)
        self.widget_button_tree_new.place(rely=0.5, y=-150)
        self.widget_button_tree_up = ttk.Button(self.frame_navigation,
                                                text="Up",
                                                command=self.button_tree_up)
        # self.widget_button_tree_up.grid(column=0, row=1)
        self.widget_button_tree_up.place(rely=0.5, y=-100)
        self.widget_button_tree_left = ttk.Button(self.frame_navigation,
                                                  text="Left",
                                                  command=self.button_tree_left)
        # self.widget_button_tree_left.grid(column=0, row=2)
        self.widget_button_tree_left.place(rely=0.5, y=-50)

        self.widget_button_tree_right = ttk.Button(self.frame_navigation,
                                                   text="Right",
                                                   command=self.button_tree_right)
        # self.widget_button_tree_right.grid(column=0, row=3)
        self.widget_button_tree_right.place(rely=0.5, y=0)
        self.widget_button_tree_down = ttk.Button(self.frame_navigation,
                                                  text="Down",
                                                  command=self.button_tree_down)
        # self.widget_button_tree_down.grid(column=0, row=4)
        self.widget_button_tree_down.place(rely=0.5, y=50)

        self.widget_button_tree_del = ttk.Button(self.frame_navigation,
                                                 text="Delete",
                                                 command=self.button_tree_del)
        # self.widget_button_tree_del.grid(column=0, row=5)
        self.widget_button_tree_del.place(rely=0.5, y=100)

        # Create the edit region
        self.widget_label_name = ttk.Label(self.frame_editregion, text="Name", padding=5)
        self.widget_label_name.grid(column=0, row=0, sticky="ew")
        self.widget_entry_name = ttk.Entry(self.frame_editregion)
        self.widget_entry_name.grid(column=1, columnspan=2, row=0, sticky="ew")

        self.widget_label_command = ttk.Label(self.frame_editregion, text="Command", padding=5)
        self.widget_label_command.grid(column=0, row=1, sticky="ew")
        self.widget_entry_command = ttk.Entry(self.frame_editregion)
        self.widget_entry_command.grid(column=1, columnspan=2, row=1, sticky="ew")

        self.entry_property = tk.IntVar(value=1)
        self.widget_labelFrame = ttk.LabelFrame(
            self.frame_editregion,
            text="Properties",
            padding=5
        )
        self.widget_labelFrame.grid(column=0, row=2, columnspan=3)
        self.widget_radiobutton_property1 = ttk.Radiobutton(
            self.widget_labelFrame,
            command=self.radiobutton_properties,
            variable=self.entry_property,
            value=1,
            text="Command"
        )
        self.widget_radiobutton_property1.grid(column=0, row=0)
        self.widget_radiobutton_property2 = ttk.Radiobutton(
            self.widget_labelFrame,
            command=self.radiobutton_properties,
            variable=self.entry_property,
            value=2,
            text="Submenu"
        )
        self.widget_radiobutton_property2.grid(column=1, row=0)
        self.widget_radiobutton_property3 = ttk.Radiobutton(
            self.widget_labelFrame,
            command=self.radiobutton_properties,
            variable=self.entry_property,
            value=3,
            text="Back"
        )
        self.widget_radiobutton_property3.grid(column=2, row=0)

        self.widget_button_apply = ttk.Button(
            self.frame_editregion,
            text="Apply",
            command=self.button_apply,
            padding=5
        )
        self.widget_button_apply.grid(column=0, row=3, sticky="ew")
        self.widget_button_cancel = ttk.Button(
            self.frame_editregion,
            text="Cancel",
            command=self.button_cancel,
            padding=5
        )
        self.widget_button_cancel.grid(column=2, row=3, sticky="ew")

        # self.widget_button_print = ttk.Button(
        #     self.frame_editregion,
        #     text="print",
        #     command=self.button_print,
        #     padding=5
        # )
        # self.widget_button_print.grid(column=2, row=4, sticky="ew")

    # def button_print(self):
    #     self.menu.print_menu()

    def change_size_treeview(self, event):
        """Change the size of the treeview."""
        # print(f"change_size_treeview: x:{event.x}/y:{event.y}")
        self.treeview_width += event.x
        self.frame_treeview.config(width=self.treeview_width)

    def release_click_treeview(self, event):
        """Event for the treeview."""
        self.widget_entry_command.configure(state="normal")
        if len(self.widget_tree.focus()) == 0:
            return
        self.widget_entry_name.delete(0, "end")
        self.widget_entry_name.insert(
            index=0,
            string=self.widget_tree.get_text()
        )
        search_id = self.widget_tree.focus()
        if search_id:
            self.actual_entry = self.menu.search(search_id)
            self.widget_entry_command.delete(0, "end")
            self.widget_entry_command.insert(
                index=0,
                string=self.actual_entry.get_command()
            )
            if self.actual_entry.get_submenu():
                self.entry_property.set(value=2)
                self.widget_entry_command.configure(state="disabled")
            elif self.actual_entry.get_backmenu():
                self.entry_property.set(value=3)
                self.widget_entry_command.configure(state="disabled")
            else:
                self.entry_property.set(value=1)

    def radiobutton_properties(self):
        """Set entry-command depending of the checkbutton state."""
        entry_property = self.entry_property.get()

        self.widget_entry_command.configure(state="normal")

        if entry_property == 1:
            pass
        elif entry_property == 2:
            self.widget_entry_command.delete(0, "end")
            self.widget_entry_command.insert(
                index=0,
                string=f"{self.widget_entry_name.get().replace(' ', '_').lower()}_menu.sh"
            )
            self.widget_entry_command.configure(state="disabled")
        else:
            if self.widget_tree.get_parentparent_name():
                temp_command = f"{self.widget_tree.get_parentparent_name().replace(' ', '_').lower()}_menu.sh"
            else:
                temp_command = "main_menu.sh"
            self.widget_entry_command.delete(0, "end")
            self.widget_entry_command.insert(
                index=0,
                string=temp_command
            )
            self.widget_entry_command.configure(state="disabled")

    def button_apply(self):
        """Change menu entry."""
        if self.actual_entry:
            new_text = self.widget_entry_name.get()
            self.radiobutton_properties()
            new_command = self.widget_entry_command.get()
            self.widget_tree.change_text(new_text)
            self.actual_entry.set_name(name=new_text)
            self.actual_entry.set_command(command=new_command)
            entry_property = self.entry_property.get()
            if entry_property == 2:
                self.actual_entry.set_submenu(submenu=True)
                if not self.actual_entry.get_next_menu():
                    self.actual_entry.set_next_menu(menu_head())
            elif entry_property == 3:
                self.actual_entry.set_backmenu(is_backmenu=True)
            # self.menu.print_menu()

    def button_cancel(self):
        """Set old values for the menu entry."""
        if self.actual_entry:
            self.widget_entry_name.delete(0, "end")
            self.widget_entry_name.insert(
                index=0,
                string=self.actual_entry.get_name()
            )
            self.widget_entry_command.delete(0, "end")
            self.widget_entry_command.insert(
                index=0,
                string=self.actual_entry.get_command()
            )

    def button_tree_new(self):
        """Add new entry in the treeview."""
        iid = 0
        new_node = menu_node()
        new_node.set_name("New Entry")
        if self.actual_entry:
            iid = self.actual_entry.get_id()
        elif self.menu.next_node:
            iid = self.menu.next_node.get_id()
        else:
            self.menu.append(new_node)
            new_id = self.menu.next_node.get_id()
        if iid:
            new_id = self.menu.insert(new_node, iid, False)
        self.widget_tree.new_entry("New Entry", new_id)
        self.release_click_treeview(None)

    def button_tree_up(self):
        """Move an entry one line up."""
        iid = self.widget_tree.entry_up()
        self.menu.move(iid[0], iid[1])

    def button_tree_down(self):
        """Move an entry one line down."""
        iid = self.widget_tree.entry_down()
        self.menu.move(iid[0], iid[1])

    def button_tree_right(self):
        """Move an entry to first child of next submenu."""
        iid = self.widget_tree.entry_right()
        if iid:
            self.menu.move(iid[0], iid[1])

    def button_tree_left(self):
        """Move an entry one level up befor parent."""
        iid = self.widget_tree.entry_left()
        if iid:
            self.menu.move(iid[0], iid[1])

    def button_tree_del(self):
        """Delete focused entry."""
        iid = self.widget_tree.entry_del()
        if iid:
            self.menu.delete(iid[0], iid[1])
