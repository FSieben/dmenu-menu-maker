#!/usr/bin/env python3
import tkinter as tk
from tkinter import ttk

from file_operation import save_option
from file_operation import load_option
from .workspace import Workspace_frame


class Option_frame(Workspace_frame):
    """Create the option frame."""

    def __init__(self, master: tk.Tk, actual_frame: tk.Frame):
        """Init for the class."""
        super().__init__(master, actual_frame)
        self.create_workspace()

    def create_workspace(self):
        """Create the workspace for the options."""
        # Create the label and entry for the option: directory
        option = load_option()
        # if "directory" not in option:
        #     option.update(
        #         {"directory": "$HOME/.config./dmenu/appmenu",
        #          "dmenurofi": 'dmenu -bw 0 -l 20 -fn "mononoki nerd font"'}
        #     )

        widget_label_directory = ttk.Label(
            self,
            text="Directory",
            padding=5,
            anchor="w"
        )
        widget_label_directory.grid(column=0, row=0, sticky="w")
        self.option_widget_entry_directory = ttk.Entry(self)
        self.option_widget_entry_directory.grid(column=1, columnspan=2, row=0)
        self.option_widget_entry_directory.insert(0, option["directory"])

        # Create the label and entry for the option: dmenu or rofi
        widget_label_dmenurofi = ttk.Label(
            self,
            text="dmenu or rofi",
            padding=5,
            anchor="w"
        )
        widget_label_dmenurofi.grid(column=0, row=1, sticky="w")
        self.option_widget_entry_dmenurofi = ttk.Entry(self)
        self.option_widget_entry_dmenurofi.grid(column=1, columnspan=2, row=1)
        self.option_widget_entry_dmenurofi.insert(0, option["dmenurofi"])

        # Create the close button.
        self.option_widget_button_ok = ttk.Button(
            self,
            text="Close",
            command=self.option_button_close)
        self.option_widget_button_ok.grid(column=2, row=2, sticky="e")

    def option_button_close(self):
        """Call when the close button will be clicked."""
        option = {}
        option.update({"directory": self.option_widget_entry_directory.get(),
                       "dmenurofi": self.option_widget_entry_dmenurofi.get()})
        save_option(option)
        self.hide()
        self.show_last_frame()
