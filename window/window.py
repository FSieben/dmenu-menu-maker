#!/usr/bin/env python3
import tkinter as tk
from tkinter import filedialog as fd

from file_operation import read_file, load_menu, save_menu
from .option import Option_frame
from .edit import Edit_frame
from lists import menu_head


class Window():
    """Creation of the window."""

    def __init__(self, master=None):
        """Init of the class."""
        self.master = master
        self.menu = None
        self.init_window()
        self.actual_entry = None
        self.filename = None

    def init_window(self):
        """Initialis of the window."""
        self.master.title("(d)Menu-Maker")

        # Create workspace frames
        # self.workspace_frame = None
        self.edit_frame = None
        self.option_frame = None

        # Create the menubar
        menubar = tk.Menu(self.master)
        self.master.config(menu=menubar)

        # Create the file menu
        file_menu = tk.Menu(menubar, tearoff=0)
        file_menu.add_command(label="New", command=self.new_function)
        file_menu.add_command(label="Load", command=self.load_function)
        file_menu.add_command(label="Save", command=self.save_function)
        file_menu.add_command(label="Save As", command=self.saveas_function)
        file_menu.add_command(label="Exit", command=self.master.quit)
        menubar.add_cascade(label="File", menu=file_menu)

        # Create the option menu
        option_menu = tk.Menu(menubar, tearoff=0)
        option_menu.add_command(label="Option", command=self.option_function)
        menubar.add_cascade(label="Option", menu=option_menu)

    def load_function(self):
        """Load file."""
        self.filename = fd.askopenfilename(
            filetypes=(
                (
                    ("menu files", "*.sh"),
                    ("all files", "*.*")
                )
            )
        )
        if len(self.filename) == 0:
            return
        self.menu = menu_head()
        self.menu = read_file(self.filename, self.menu)
        load_menu("/".join(self.filename.split("/")[:-1]) + "/", self.menu)
        if self.edit_frame:
            self.edit_frame.destroy()

        self.edit_frame = Edit_frame(self.master, None, self.menu)

    def new_function(self):
        """Create a new menu."""
        self.menu = menu_head()

        if self.edit_frame:
            self.edit_frame.destroy()
        if self.option_frame:
            self.option_frame.hide()

        self.edit_frame = Edit_frame(self.master, None, self.menu)

    def option_function(self):
        """Call when option menu will be clicked."""
        self.hide_frames()
        if not self.option_frame:
            self.option_frame = Option_frame(self.master, self.edit_frame)
        else:
            self.option_frame.show(self.edit_frame)

    def hide_frames(self):
        """Hide all frames."""
        if self.edit_frame:
            self.edit_frame.pack_forget()

        # self.menu.print_menu()

    def save_function(self):
        """Save linked list."""
        if self.filename:
            save_menu("/".join(self.filename.split("/")[:-1])+"/", name="main_menu.sh", menu=self.menu)

    def saveas_function(self):
        """Save as... linked list."""
        save_directory = fd.askdirectory(title="Save as...")
        # print(save_directory)
        if save_directory:
            save_menu(save_directory+"/", name="main_menu.sh", menu=self.menu)
