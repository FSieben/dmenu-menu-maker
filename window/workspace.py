#!/usr/bin/env python3
import tkinter as tk


class Workspace_frame(tk.Frame):
    """Create a frame for the workspace."""

    def __init__(self, master, actual_frame: tk.Frame):
        """Init for the class."""
        super().__init__(master)
        self.last_frame = actual_frame
        self.show()

    def show(self, actual_frame=None):
        """Show the frame."""
        if actual_frame:
            self.last_frame = actual_frame
        self.pack(side="top", fill="both", expand=True)

    def hide(self):
        """Hide the frame."""
        self.pack_forget()

    def show_last_frame(self):
        """Show the last frame again."""
        if self.last_frame:
            self.last_frame.pack(side="top", fill="both", expand=True)
