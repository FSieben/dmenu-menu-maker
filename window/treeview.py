#!/usr/bin/env python3
from tkinter import ttk
import tkinter as tk


class myTreeview(ttk.Treeview):
    """Create a Treeview.

    Implement some helper methods.
    """

    def __init__(self, *arg, **kwarg):
        """Init this class.

        Load the __init__ from the original class.
        """
        super().__init__(*arg, **kwarg)
        self.menu = None

    def set_menu(self, Menu):
        """Set the menu."""
        self.menu = Menu

    def fill_treeview(self, menu=None, parent=""):
        """Put all entries from linked list into treeview.

        menu: The menu as a linked list.
        parent: Define the parent of the menu entry.
        """
        if menu is not None:
            temp_menu = menu
        else:
            temp_menu = self.menu.next_node
        while temp_menu is not None:
            parent_id = self.insert(
                parent,
                tk.END,
                iid=temp_menu.get_id(),
                text=temp_menu.get_name()
            )
            # print("menu: " + temp_menu.name)
            if temp_menu.get_submenu():
                # print("submenu: " + temp_menu.name)
                self.fill_treeview(temp_menu.next_menu.next_node, parent_id)
            temp_menu = temp_menu.next_node

    def change_text(self, new_text):
        """Change text of a treeview entry.

        new_text: New text for the entry which has focus.
        """
        self.item(self.focus(), text=new_text)

    def get_text(self):
        """Get name of a treeview entry.

        Return the entry name of the entry which has focus.
        """
        return self.item(self.focus())["text"]

    def get_id(self, event):
        """Get the id of the treeview entry.

        Return the id of the entry which has focus.
        event: The event handler.
        """
        return self.identify("item", event.x, event.y)

    def get_prev_id(self):
        """Get id of previous item."""
        actual_item = self.focus()
        return self.prev(actual_item)

    def new_entry(self, text, iid):
        """Add new tree entry.

        Add a new entry in the treeview below the focus entry.
        text: Name of the entry.
        """
        actual_item = self.focus()
        actual_parent = self.parent(actual_item)
        actual_index = self.index(actual_item)
        self.insert(actual_parent, actual_index+1, iid=iid, text=text)
        self.focus(iid)
        self.selection_set(iid)

    def entry_up(self):
        """Move selected entry one element up.

        Return: [old_prev_id, new_prev_id, if_child]
            old_prev_id: Id of old prev element.
            new_prev_id: Id of new prev element.
            if_child: If first child of new_prev_id (True/False)
        """
        item = self.focus()
        old_iid = self._previous_id(item)
        if old_iid[1]:
            index = self.index(old_iid[0])+1
            parent = self.parent(old_iid[0])
        else:
            index = self.index(item)
            parent = self.parent(item)

        self.move(item, parent, index-1)
        new_iid = self._previous_id(item)

        return [old_iid, new_iid]

    def entry_down(self):
        """Move selected entry one element down."""
        item = self.focus()
        old_iid = self._previous_id(item)
        index = self.index(item)
        parent = self.parent(item)
        if not self.next(item) and self.parent(item):
            index = self.index(parent)
            parent = self.parent(parent)

        self.move(item, parent, index+1)
        new_iid = self._previous_id(item)

        return [old_iid, new_iid]

    def entry_right(self):
        """Move the entry to first child of next submenu."""
        item = self.focus()
        old_iid = self._previous_id(item)
        next_iid = self.next(item)
        entry = self.menu.search(next_iid)
        if entry.get_submenu():
            self.move(item, next_iid, 0)
            return [old_iid, [next_iid, True]]

    def entry_left(self):
        """Move the entry one leve up befor parent."""
        item = self.focus()
        old_iid = self._previous_id(item)
        parent_id = self.parent(item)
        parent_index = self.index(parent_id)
        # print(old_iid, parent_id, parent_index)
        if parent_id:
            self.move(item, self.parent(parent_id), parent_index)
            new_iid = self.prev(item)
            if new_iid:
                # print(f"New_iid:{new_iid}, return value {old_iid, [new_iid, False]}")
                return [old_iid, [new_iid, False]]
            else:
                # print(f"New_iid:{new_iid}, return value {old_iid, [new_iid, False]}")
                return [old_iid, [self.parent(item), True]]

    def get_parent_name(self):
        """Return the name of the parent."""
        parent_id = self.parent(self.focus())
        if parent_id:
            return self.item(parent_id)["text"]
        else:
            return None

    def get_parentparent_name(self):
        """Return the name of the parent from the parent."""
        parent_id = self.parent(self.focus())
        parentparent_id = self.parent(parent_id)
        if parentparent_id:
            return self.item(parentparent_id)["text"]
        else:
            return None

    def entry_del(self):
        """Delete the focused entry."""
        item = self.focus()
        old_iid = self._previous_id(item)
        self.delete(item)

        return old_iid

    def _previous_id(self, focus_iid):
        """Find id from previous item.

        Return: iid from previous item.
        """
        previous_iid = self.prev(focus_iid)
        is_first_child = False
        if not previous_iid:
            previous_iid = self.parent(focus_iid)
            is_first_child = True

        return [previous_iid, is_first_child]
